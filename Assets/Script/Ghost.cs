﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ghost : MonoBehaviour
{
    NavMeshAgent _navAgent;
    GameObject _player;
    Pacman _Pacman;
    [SerializeField]
    GameObject GhostMesh;

    static public int Points =200;

    void Awake()
    {
        _navAgent = GetComponent<NavMeshAgent>();
    }

    private void OnDisable()        //si attiva quando il fantasma viene distrutto
    {
        Destroy(GhostMesh);
    }
    // Use this for initialization
    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player");   //i comandi che iniziano con find sono molto costosi soprattutto se ci sono molti oggetti
        if (_player == null)
            throw new UnityException("Player is missing");
        _Pacman = _player.GetComponent<Pacman>();
        //_navAgent.SetDestination(_player.transform.position);
    }

    // Update is called once per frame
    void Update()
    {
        if (_Pacman.IsGameReady() == false)
            return;

        GhostMesh.transform.position = transform.position;
        _navAgent.SetDestination(_player.transform.position);
    }
}
