﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : pill
{
    bool working = false;
    //public int Points;        lo cancello perchè points è definito in pill di cui questo script è ereditario
	// Use this for initialization
	void Start () {
        StartCoroutine(Animate());
        //StartCoroutine("Animate");  possibile alternativa. Se devo introdurre dei parametri allora ("Animate", 5) oppure (Animate(5))
    }

    IEnumerator Animate()
    {
        working = true;
        float waitTime = 0.1f;

        while (working==true)
        {
            transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            yield return new WaitForSeconds(waitTime);
            transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);
            yield return new WaitForSeconds(waitTime);
            transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            yield return new WaitForSeconds(waitTime);
            transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            yield return new WaitForSeconds(waitTime);
            transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            yield return new WaitForSeconds(waitTime);
            transform.localScale = new Vector3(1f, 1f, 1f);
            yield return new WaitForSeconds(waitTime);
            transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            yield return new WaitForSeconds(waitTime);
            transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            yield return new WaitForSeconds(waitTime);

            //Debug.Log("ANIMATION IN PROGRESS");

            // ALTERNATIVA senza waitTime
            //transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            //yield return new WaitForSeconds(0.5f);      //yield parola che va in coppia con gli enumeratori. waitforsecond gli dice di aspettare prima di fare la nuova funzione (intanto esce dal contatore e fa altro)
            //transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            //yield return new WaitForSeconds(0.5f);      //se invece di new waitforsecond scrivevo null faceva la riga dopo al frame successivo
            //transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            //yield return new WaitForSeconds(0.5f);
        }
    }
    // Update is called once per frame
    void Update () {
		
	}
}
