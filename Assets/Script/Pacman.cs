﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pacman : MonoBehaviour
{
    [SerializeField]
    float MovementSpeed;
    public int TotalPoints;
    public int Lives =3;
    AudioSource _audioSource;
    [SerializeField]
    AudioClip _audioMov;
    [SerializeField]
    AudioClip _audioDeath;
    [SerializeField]
    AudioClip _audioStart;
    bool _hasPowerUp;
    /// <summary>
    /// tempo trascorso da quando si è raccolto un power up
    /// </summary>
    float _powerUpElapsedTime = 0;
    /// <summary>
    /// indica la durata dell'ultimo power up raccolto
    /// </summary>
    float _PowerupDuration=10;
    /// <summary>
    /// numero di fantasmi mangiati durante l'effetto del powerup
    /// </summary>
    int _eatenGhost;
    bool _isGameReady;

    public bool IsGameReady()   //non una variabile pubblica ma un metodo pubblico che da come ritorno la variabile privata
    {
        return _isGameReady;
    }

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start()
    {
        StartCoroutine(CheckStartMusic());
        
    }

    /// <summary>
    /// aspetta che la musica di inzio sia terminata prima di permettere l'avvio del gioco
    /// </summary>
    /// <returns></returns>
    IEnumerator CheckStartMusic()
    {
        _isGameReady = false;
        _audioSource.PlayOneShot(_audioStart);
        while (_audioSource.isPlaying)
            yield return null;
        _isGameReady = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (_isGameReady == false)
            return;
        else
        {
            float h = Input.GetAxis("Horizontal");
            float v = Input.GetAxis("Vertical");
            //Debug.Log("h: " + h + " - v: " + v);

            if (h != 0)
                transform.Translate(Vector3.right * MovementSpeed * h);     //h regola la velocità
            else
                transform.Translate(Vector3.forward * MovementSpeed * v);     //h regola la velocità

            if (h != 0 || v != 0)
                if (!_audioSource.isPlaying)
                {
                    _audioSource.PlayOneShot(_audioMov);
                }

                if (_hasPowerUp == true)
                {
                    _powerUpElapsedTime += Time.deltaTime;      //aumento il tempo del frame
                    Debug.Log("ELAPSED TIME " + _powerUpElapsedTime);
                    if (_powerUpElapsedTime >= _PowerupDuration)
                        _hasPowerUp = false;
                }
        }
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "pill")
        {
            OnEatPill(other);
        }

        if (other.gameObject.tag=="ghost")
        {
            if (!_hasPowerUp)
                OnHit();
            else
            {
                try         //tutta la parte che può generare eccezioni
                {
                    OnEatGhost(other);
                }
                catch       //qui vengono gestite le eccezzioni
                {

                }
                finally
                {

                }
            }
        }
    }

    /// <summary>
    /// codice di quando mangio il fantasma
    /// </summary>
    /// <param name="other"></param>
    private void OnEatGhost(Collider other)
    {
        if (other == null)
            throw new System.ArgumentNullException("other", "other cannot be null");
        _eatenGhost++;
        //(int)Mathf.Pow(x, y) significa crea un int facendo x^y (se non dici (int) ti restituisce un double)(anche x e y li prende come double)
        TotalPoints += Ghost.Points * (int)Mathf.Pow(2, _eatenGhost - 1);
        Destroy(other.gameObject);
    }

    /// <summary>
    /// codice eseguito quando si mangia la pillola
    /// </summary>
    /// <param name="other"></param>
    void OnEatPill(Collider other)
    {
        Debug.Log("GNAMM!");
        pill pill = other.gameObject.GetComponent<pill>();
        //pill pill=other.gameObject.GetComponent(typeof(pill)); possibile alternativa             //getcompoment mi consente di andare a riprendere le proprietà di un altro oggetto
        TotalPoints += pill.Points;

        if (pill is PowerUp)
        {
            Debug.Log("POWER UP!!");
            _hasPowerUp = true;
            _powerUpElapsedTime = 0;
            _eatenGhost = 0;
        }
        Destroy(other.gameObject);
    }

    /// <summary>
    /// codice eseguito quando pacman viene colpito da un fantasma
    /// </summary>
    void OnHit()
    {
        Debug.Log("GAME OVER");
        if (!_audioSource.isPlaying)
        {
            _audioSource.Stop();
        }
        _audioSource.PlayOneShot(_audioDeath);
        Lives -= 1;
    }
}
